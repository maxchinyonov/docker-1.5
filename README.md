<h1 class="code-line" data-line-start=0 data-line-end=1 ><a id="__0"></a>Описание задания</h1>
<ol>
<li class="has-line-data" data-line-start="2" data-line-end="9">Запустить сборку имеджа внутри контейнера<br>
ОС и содержание Докер файла по желанию.<br>
а) с приввелегиями<br>
б) с пробросом сокета<br>
Сам докер файл закинуть двумя способами<br>
а) монтированием при старте контейнера<br>
б) копировать в запущенный контейнер (docker cp)</li>
</ol>
<p class="has-line-data" data-line-start="11" data-line-end="12">В ответе - скрин консоли.</p>
<p class="has-line-data" data-line-start="13" data-line-end="15">Задача на 100 баллов<br>
Сделать самим образ с докер сервисом внутри на основе дебиана.</p>





<h1 class="code-line" data-line-start=0 data-line-end=1 ><a id="__0"></a>Сборка</h1>

<pre><code class="has-line-data" data-line-start="1" data-line-end="3" class="language-sh">[root@localhost lesson1.<span class="hljs-number">5</span>]<span class="hljs-comment"># cat Dockerfile3</span>
</code></pre>
<p class="has-line-data" data-line-start="3" data-line-end="5">FROM debian:9<br>
RUN apt update &amp;&amp; apt install -y vim strace</p>
<pre><code class="has-line-data" data-line-start="6" data-line-end="8" class="language-sh">[root@localhost lesson1.<span class="hljs-number">5</span>]<span class="hljs-comment"># cat Dockerfile</span>
</code></pre>
<p class="has-line-data" data-line-start="8" data-line-end="14">FROM debian:9<br>
RUN apt-get update &amp;&amp; apt-get install -y apt-transport-https ca-certificates curl gnupg lsb-release procps<br>
RUN curl -fsSL <a href="https://download.docker.com/linux/debian/gpg">https://download.docker.com/linux/debian/gpg</a> | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg<br>
RUN echo  “deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] <a href="https://download.docker.com/linux/debian">https://download.docker.com/linux/debian</a> <br>
$(lsb_release -cs) stable” | tee /etc/apt/sources.list.d/docker.list &gt; /dev/null<br>
RUN apt-get update &amp;&amp; apt-get install -y docker-ce docker-ce-cli <a href="http://containerd.io">containerd.io</a> strace</p>
<pre><code class="has-line-data" data-line-start="15" data-line-end="17" class="language-sh">[root@localhost lesson1.<span class="hljs-number">5</span>]<span class="hljs-comment"># docker build -t obraz:base -f Dockerfile . </span>
</code></pre>
<p class="has-line-data" data-line-start="17" data-line-end="495">Sending build context to Docker daemon  3.584kB<br>
Step 1/5 : FROM debian:9<br>
—&gt; d74a4ce6ed8b<br>
Step 2/5 : RUN apt-get update &amp;&amp; apt-get install -y apt-transport-https ca-certificates curl gnupg lsb-release procps<br>
—&gt; Using cache<br>
—&gt; ab7a398b3d4e<br>
Step 3/5 : RUN curl -fsSL <a href="https://download.docker.com/linux/debian/gpg">https://download.docker.com/linux/debian/gpg</a> | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg<br>
—&gt; Using cache<br>
—&gt; 959ef5887c80<br>
Step 4/5 : RUN echo  “deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] <a href="https://download.docker.com/linux/debian">https://download.docker.com/linux/debian</a>  $(lsb_release -cs) stable” | tee /etc/apt/sources.list.d/docker.list &gt; /dev/null<br>
—&gt; Using cache<br>
—&gt; 4c964184613d<br>
Step 5/5 : RUN apt-get update &amp;&amp; apt-get install -y docker-ce docker-ce-cli <a href="http://containerd.io">containerd.io</a> strace<br>
—&gt; Running in 5e527b7bbe50<br>
Hit:1 <a href="http://security.debian.org/debian-security">http://security.debian.org/debian-security</a> stretch/updates InRelease<br>
Ign:2 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch InRelease<br>
Hit:3 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch-updates InRelease<br>
Hit:4 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch Release<br>
Get:5 <a href="https://download.docker.com/linux/debian">https://download.docker.com/linux/debian</a> stretch InRelease [44.8 kB]<br>
Get:7 <a href="https://download.docker.com/linux/debian">https://download.docker.com/linux/debian</a> stretch/stable amd64 Packages [15.9 kB]<br>
Fetched 60.7 kB in 1s (59.6 kB/s)<br>
Reading package lists…<br>
Reading package lists…<br>
Building dependency tree…<br>
Reading state information…<br>
The following additional packages will be installed:<br>
aufs-dkms aufs-tools binutils cgroupfs-mount cpp cpp-6 dkms dmsetup fakeroot<br>
gcc gcc-6 git git-man iptables kmod less libasan3 libatomic1 libbsd0<br>
libc-dev-bin libc6-dev libcc1-0 libcilkrts5 libdevmapper1.02.1 libedit2<br>
liberror-perl libfakeroot libgcc-6-dev libgdbm3 libgomp1 libip4tc0 libip6tc0<br>
libiptc0 libisl15 libitm1 libkmod2 liblsan0 libltdl7 libmpc3 libmpfr4<br>
libmpx2 libnetfilter-conntrack3 libnfnetlink0 libperl5.24 libpopt0<br>
libquadmath0 libseccomp2 libtsan0 libubsan0 libx11-6 libx11-data libxau6<br>
libxcb1 libxdmcp6 libxext6 libxmuu1 libxtables12 linux-compiler-gcc-6-x86<br>
linux-headers-4.9.0-16-amd64 linux-headers-4.9.0-16-common<br>
linux-headers-amd64 linux-kbuild-4.9 linux-libc-dev make manpages<br>
manpages-dev netbase openssh-client patch perl perl-modules-5.24 pigz rename<br>
rsync sudo xauth<br>
Suggested packages:<br>
aufs-dev binutils-doc cpp-doc gcc-6-locales python3-apport menu gcc-multilib<br>
autoconf automake libtool flex bison gdb gcc-doc gcc-6-multilib gcc-6-doc<br>
libgcc1-dbg libgomp1-dbg libitm1-dbg libatomic1-dbg libasan3-dbg<br>
liblsan0-dbg libtsan0-dbg libubsan0-dbg libcilkrts5-dbg libmpx2-dbg<br>
libquadmath0-dbg gettext-base git-daemon-run | git-daemon-sysvinit git-doc<br>
git-el git-email git-gui gitk gitweb git-arch git-cvs git-mediawiki git-svn<br>
glibc-doc make-doc man-browser keychain libpam-ssh monkeysphere ssh-askpass<br>
ed diffutils-doc perl-doc libterm-readline-gnu-perl<br>
| libterm-readline-perl-perl openssh-server<br>
The following NEW packages will be installed:<br>
aufs-dkms aufs-tools binutils cgroupfs-mount <a href="http://containerd.io">containerd.io</a> cpp cpp-6 dkms<br>
dmsetup docker-ce docker-ce-cli fakeroot gcc gcc-6 git git-man iptables kmod<br>
less libasan3 libatomic1 libbsd0 libc-dev-bin libc6-dev libcc1-0 libcilkrts5<br>
libdevmapper1.02.1 libedit2 liberror-perl libfakeroot libgcc-6-dev libgdbm3<br>
libgomp1 libip4tc0 libip6tc0 libiptc0 libisl15 libitm1 libkmod2 liblsan0<br>
libltdl7 libmpc3 libmpfr4 libmpx2 libnetfilter-conntrack3 libnfnetlink0<br>
libperl5.24 libpopt0 libquadmath0 libseccomp2 libtsan0 libubsan0 libx11-6<br>
libx11-data libxau6 libxcb1 libxdmcp6 libxext6 libxmuu1 libxtables12<br>
linux-compiler-gcc-6-x86 linux-headers-4.9.0-16-amd64<br>
linux-headers-4.9.0-16-common linux-headers-amd64 linux-kbuild-4.9<br>
linux-libc-dev make manpages manpages-dev netbase openssh-client patch perl<br>
perl-modules-5.24 pigz rename rsync strace sudo xauth<br>
0 upgraded, 80 newly installed, 0 to remove and 0 not upgraded.<br>
Need to get 153 MB of archives.<br>
After this operation, 692 MB of additional disk space will be used.<br>
Get:1 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 perl-modules-5.24 all 5.24.1-3+deb9u7 [2723 kB]<br>
Get:2 <a href="http://security.debian.org/debian-security">http://security.debian.org/debian-security</a> stretch/updates/main amd64 libbsd0 amd64 0.8.3-1+deb9u1 [82.9 kB]<br>
Get:3 <a href="http://security.debian.org/debian-security">http://security.debian.org/debian-security</a> stretch/updates/main amd64 linux-kbuild-4.9 amd64 4.9.272-2 [979 kB]<br>
Get:4 <a href="https://download.docker.com/linux/debian">https://download.docker.com/linux/debian</a> stretch/stable amd64 <a href="http://containerd.io">containerd.io</a> amd64 1.4.3-1 [28.1 MB]<br>
Get:5 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libgdbm3 amd64 1.8.3-14 [30.0 kB]<br>
Get:6 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libperl5.24 amd64 5.24.1-3+deb9u7 [3527 kB]<br>
Get:7 <a href="http://security.debian.org/debian-security">http://security.debian.org/debian-security</a> stretch/updates/main amd64 linux-libc-dev amd64 4.9.272-2 [1589 kB]<br>
Get:8 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 perl amd64 5.24.1-3+deb9u7 [218 kB]<br>
Get:9 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 liberror-perl all 0.17024-1 [26.9 kB]<br>
Get:10 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 git-man all 1:2.11.0-3+deb9u7 [1436 kB]<br>
Get:11 <a href="http://security.debian.org/debian-security">http://security.debian.org/debian-security</a> stretch/updates/main amd64 libx11-data all 2:1.6.4-3+deb9u4 [291 kB]<br>
Get:12 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 git amd64 1:2.11.0-3+deb9u7 [4170 kB]<br>
Get:13 <a href="http://security.debian.org/debian-security">http://security.debian.org/debian-security</a> stretch/updates/main amd64 libx11-6 amd64 2:1.6.4-3+deb9u4 [748 kB]<br>
Get:14 <a href="http://security.debian.org/debian-security">http://security.debian.org/debian-security</a> stretch/updates/main amd64 linux-compiler-gcc-6-x86 amd64 4.9.272-2 [772 kB]<br>
Get:15 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libnfnetlink0 amd64 1.0.1-3 [13.5 kB]<br>
Get:16 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libxau6 amd64 1:1.0.8-1 [20.7 kB]<br>
Get:17 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 pigz amd64 2.3.4-1 [55.4 kB]<br>
Get:18 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libip4tc0 amd64 1.6.0+snapshot20161117-6 [67.8 kB]<br>
Get:19 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libip6tc0 amd64 1.6.0+snapshot20161117-6 [68.1 kB]<br>
Get:20 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libiptc0 amd64 1.6.0+snapshot20161117-6 [57.5 kB]<br>
Get:21 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libxtables12 amd64 1.6.0+snapshot20161117-6 [75.9 kB]<br>
Get:22 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libnetfilter-conntrack3 amd64 1.0.6-2 [38.7 kB]<br>
Get:23 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 iptables amd64 1.6.0+snapshot20161117-6 [288 kB]<br>
Get:24 <a href="http://security.debian.org/debian-security">http://security.debian.org/debian-security</a> stretch/updates/main amd64 linux-headers-4.9.0-16-common all 4.9.272-2 [7834 kB]<br>
Get:25 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libkmod2 amd64 23-2 [48.1 kB]<br>
Get:26 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 kmod amd64 23-2 [85.7 kB]<br>
Get:27 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libpopt0 amd64 1.16-10+b2 [49.4 kB]<br>
Get:28 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 netbase all 5.4 [19.1 kB]<br>
Get:29 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 less amd64 481-2.1 [126 kB]<br>
Get:30 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libedit2 amd64 3.1-20160903-3 [84.8 kB]<br>
Get:31 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 manpages all 4.10-2 [1222 kB]<br>
Get:32 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 openssh-client amd64 1:7.4p1-10+deb9u7 [780 kB]<br>
Get:33 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libisl15 amd64 0.18-1 [564 kB]<br>
Get:34 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libmpfr4 amd64 3.1.5-1 [556 kB]<br>
Get:35 <a href="http://security.debian.org/debian-security">http://security.debian.org/debian-security</a> stretch/updates/main amd64 linux-headers-4.9.0-16-amd64 amd64 4.9.272-2 [450 kB]<br>
Get:36 <a href="http://security.debian.org/debian-security">http://security.debian.org/debian-security</a> stretch/updates/main amd64 linux-headers-amd64 amd64 4.9+80+deb9u14 [6234 B]<br>
Get:37 <a href="http://security.debian.org/debian-security">http://security.debian.org/debian-security</a> stretch/updates/main amd64 sudo amd64 1.8.19p1-2.1+deb9u3 [1055 kB]<br>
Get:38 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libmpc3 amd64 1.0.3-1+b2 [39.9 kB]<br>
Get:39 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 cpp-6 amd64 6.3.0-18+deb9u1 [6584 kB]<br>
Get:40 <a href="https://download.docker.com/linux/debian">https://download.docker.com/linux/debian</a> stretch/stable amd64 docker-ce-cli amd64 5:19.03.15<sub>3-0</sub>debian-stretch [44.2 MB]<br>
Get:41 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 cpp amd64 4:6.3.0-4 [18.7 kB]<br>
Get:42 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libcc1-0 amd64 6.3.0-18+deb9u1 [30.6 kB]<br>
Get:43 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 binutils amd64 2.28-5 [3770 kB]<br>
Get:44 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libgomp1 amd64 6.3.0-18+deb9u1 [73.3 kB]<br>
Get:45 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libitm1 amd64 6.3.0-18+deb9u1 [27.3 kB]<br>
Get:46 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libatomic1 amd64 6.3.0-18+deb9u1 [8966 B]<br>
Get:47 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libasan3 amd64 6.3.0-18+deb9u1 [311 kB]<br>
Get:48 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 liblsan0 amd64 6.3.0-18+deb9u1 [115 kB]<br>
Get:49 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libtsan0 amd64 6.3.0-18+deb9u1 [257 kB]<br>
Get:50 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libubsan0 amd64 6.3.0-18+deb9u1 [107 kB]<br>
Get:51 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libcilkrts5 amd64 6.3.0-18+deb9u1 [40.5 kB]<br>
Get:52 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libmpx2 amd64 6.3.0-18+deb9u1 [11.2 kB]<br>
Get:53 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libquadmath0 amd64 6.3.0-18+deb9u1 [131 kB]<br>
Get:54 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libgcc-6-dev amd64 6.3.0-18+deb9u1 [2296 kB]<br>
Get:55 <a href="https://download.docker.com/linux/debian">https://download.docker.com/linux/debian</a> stretch/stable amd64 docker-ce amd64 5:19.03.15<sub>3-0</sub>debian-stretch [22.7 MB]<br>
Get:56 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 gcc-6 amd64 6.3.0-18+deb9u1 [6900 kB]<br>
Get:57 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 gcc amd64 4:6.3.0-4 [5196 B]<br>
Get:58 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 make amd64 4.1-9.1 [302 kB]<br>
Get:59 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 patch amd64 2.7.5-1+deb9u2 [112 kB]<br>
Get:60 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 dkms all 2.3-2 [74.8 kB]<br>
Get:61 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 aufs-dkms amd64 4.9+20161219-1 [169 kB]<br>
Get:62 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 aufs-tools amd64 1:4.1+20161219-1 [102 kB]<br>
Get:63 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 cgroupfs-mount all 1.3 [5716 B]<br>
Get:64 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libseccomp2 amd64 2.3.1-2.1+deb9u1 [40.7 kB]<br>
Get:65 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libdevmapper1.02.1 amd64 2:1.02.137-2 [170 kB]<br>
Get:66 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 dmsetup amd64 2:1.02.137-2 [107 kB]<br>
Get:67 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libfakeroot amd64 1.21-3.1 [45.7 kB]<br>
Get:68 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 fakeroot amd64 1.21-3.1 [85.6 kB]<br>
Get:69 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libc-dev-bin amd64 2.24-11+deb9u4 [259 kB]<br>
Get:70 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libc6-dev amd64 2.24-11+deb9u4 [2364 kB]<br>
Get:71 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libltdl7 amd64 2.4.6-2 [389 kB]<br>
Get:72 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libxdmcp6 amd64 1:1.1.2-3 [26.3 kB]<br>
Get:73 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libxcb1 amd64 1.12-1 [133 kB]<br>
Get:74 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libxext6 amd64 2:1.3.3-1+b2 [52.5 kB]<br>
Get:75 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libxmuu1 amd64 2:1.1.2-2 [23.5 kB]<br>
Get:76 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 manpages-dev all 4.10-2 [2145 kB]<br>
Get:77 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 rename all 0.20-4 [12.5 kB]<br>
Get:78 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 rsync amd64 3.1.2-1+deb9u2 [393 kB]<br>
Get:79 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 strace amd64 4.15-2 [533 kB]<br>
Get:80 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 xauth amd64 1:1.0.9-1+b2 [39.6 kB]<br>
debconf: delaying package configuration, since apt-utils is not installed<br>
Fetched 153 MB in 1min 19s (1937 kB/s)<br>
Selecting previously unselected package perl-modules-5.24.<br>
(Reading database … 8554 files and directories currently installed.)<br>
Preparing to unpack …/00-perl-modules-5.24_5.24.1-3+deb9u7_all.deb …<br>
Unpacking perl-modules-5.24 (5.24.1-3+deb9u7) …<br>
Selecting previously unselected package libgdbm3:amd64.<br>
Preparing to unpack …/01-libgdbm3_1.8.3-14_amd64.deb …<br>
Unpacking libgdbm3:amd64 (1.8.3-14) …<br>
Selecting previously unselected package libperl5.24:amd64.<br>
Preparing to unpack …/02-libperl5.24_5.24.1-3+deb9u7_amd64.deb …<br>
Unpacking libperl5.24:amd64 (5.24.1-3+deb9u7) …<br>
Selecting previously unselected package perl.<br>
Preparing to unpack …/03-perl_5.24.1-3+deb9u7_amd64.deb …<br>
Unpacking perl (5.24.1-3+deb9u7) …<br>
Selecting previously unselected package liberror-perl.<br>
Preparing to unpack …/04-liberror-perl_0.17024-1_all.deb …<br>
Unpacking liberror-perl (0.17024-1) …<br>
Selecting previously unselected package git-man.<br>
Preparing to unpack …/05-git-man_1%3a2.11.0-3+deb9u7_all.deb …<br>
Unpacking git-man (1:2.11.0-3+deb9u7) …<br>
Selecting previously unselected package git.<br>
Preparing to unpack …/06-git_1%3a2.11.0-3+deb9u7_amd64.deb …<br>
Unpacking git (1:2.11.0-3+deb9u7) …<br>
Selecting previously unselected package libnfnetlink0:amd64.<br>
Preparing to unpack …/07-libnfnetlink0_1.0.1-3_amd64.deb …<br>
Unpacking libnfnetlink0:amd64 (1.0.1-3) …<br>
Selecting previously unselected package libxau6:amd64.<br>
Preparing to unpack …/08-libxau6_1%3a1.0.8-1_amd64.deb …<br>
Unpacking libxau6:amd64 (1:1.0.8-1) …<br>
Selecting previously unselected package pigz.<br>
Preparing to unpack …/09-pigz_2.3.4-1_amd64.deb …<br>
Unpacking pigz (2.3.4-1) …<br>
Selecting previously unselected package libip4tc0:amd64.<br>
Preparing to unpack …/10-libip4tc0_1.6.0+snapshot20161117-6_amd64.deb …<br>
Unpacking libip4tc0:amd64 (1.6.0+snapshot20161117-6) …<br>
Selecting previously unselected package libip6tc0:amd64.<br>
Preparing to unpack …/11-libip6tc0_1.6.0+snapshot20161117-6_amd64.deb …<br>
Unpacking libip6tc0:amd64 (1.6.0+snapshot20161117-6) …<br>
Selecting previously unselected package libiptc0:amd64.<br>
Preparing to unpack …/12-libiptc0_1.6.0+snapshot20161117-6_amd64.deb …<br>
Unpacking libiptc0:amd64 (1.6.0+snapshot20161117-6) …<br>
Selecting previously unselected package libxtables12:amd64.<br>
Preparing to unpack …/13-libxtables12_1.6.0+snapshot20161117-6_amd64.deb …<br>
Unpacking libxtables12:amd64 (1.6.0+snapshot20161117-6) …<br>
Selecting previously unselected package libnetfilter-conntrack3:amd64.<br>
Preparing to unpack …/14-libnetfilter-conntrack3_1.0.6-2_amd64.deb …<br>
Unpacking libnetfilter-conntrack3:amd64 (1.0.6-2) …<br>
Selecting previously unselected package iptables.<br>
Preparing to unpack …/15-iptables_1.6.0+snapshot20161117-6_amd64.deb …<br>
Unpacking iptables (1.6.0+snapshot20161117-6) …<br>
Selecting previously unselected package libkmod2:amd64.<br>
Preparing to unpack …/16-libkmod2_23-2_amd64.deb …<br>
Unpacking libkmod2:amd64 (23-2) …<br>
Selecting previously unselected package kmod.<br>
Preparing to unpack …/17-kmod_23-2_amd64.deb …<br>
Unpacking kmod (23-2) …<br>
Selecting previously unselected package libpopt0:amd64.<br>
Preparing to unpack …/18-libpopt0_1.16-10+b2_amd64.deb …<br>
Unpacking libpopt0:amd64 (1.16-10+b2) …<br>
Selecting previously unselected package netbase.<br>
Preparing to unpack …/19-netbase_5.4_all.deb …<br>
Unpacking netbase (5.4) …<br>
Selecting previously unselected package less.<br>
Preparing to unpack …/20-less_481-2.1_amd64.deb …<br>
Unpacking less (481-2.1) …<br>
Selecting previously unselected package libbsd0:amd64.<br>
Preparing to unpack …/21-libbsd0_0.8.3-1+deb9u1_amd64.deb …<br>
Unpacking libbsd0:amd64 (0.8.3-1+deb9u1) …<br>
Selecting previously unselected package libedit2:amd64.<br>
Preparing to unpack …/22-libedit2_3.1-20160903-3_amd64.deb …<br>
Unpacking libedit2:amd64 (3.1-20160903-3) …<br>
Selecting previously unselected package manpages.<br>
Preparing to unpack …/23-manpages_4.10-2_all.deb …<br>
Unpacking manpages (4.10-2) …<br>
Selecting previously unselected package openssh-client.<br>
Preparing to unpack …/24-openssh-client_1%3a7.4p1-10+deb9u7_amd64.deb …<br>
Unpacking openssh-client (1:7.4p1-10+deb9u7) …<br>
Selecting previously unselected package libisl15:amd64.<br>
Preparing to unpack …/25-libisl15_0.18-1_amd64.deb …<br>
Unpacking libisl15:amd64 (0.18-1) …<br>
Selecting previously unselected package libmpfr4:amd64.<br>
Preparing to unpack …/26-libmpfr4_3.1.5-1_amd64.deb …<br>
Unpacking libmpfr4:amd64 (3.1.5-1) …<br>
Selecting previously unselected package libmpc3:amd64.<br>
Preparing to unpack …/27-libmpc3_1.0.3-1+b2_amd64.deb …<br>
Unpacking libmpc3:amd64 (1.0.3-1+b2) …<br>
Selecting previously unselected package cpp-6.<br>
Preparing to unpack …/28-cpp-6_6.3.0-18+deb9u1_amd64.deb …<br>
Unpacking cpp-6 (6.3.0-18+deb9u1) …<br>
Selecting previously unselected package cpp.<br>
Preparing to unpack …/29-cpp_4%3a6.3.0-4_amd64.deb …<br>
Unpacking cpp (4:6.3.0-4) …<br>
Selecting previously unselected package libcc1-0:amd64.<br>
Preparing to unpack …/30-libcc1-0_6.3.0-18+deb9u1_amd64.deb …<br>
Unpacking libcc1-0:amd64 (6.3.0-18+deb9u1) …<br>
Selecting previously unselected package binutils.<br>
Preparing to unpack …/31-binutils_2.28-5_amd64.deb …<br>
Unpacking binutils (2.28-5) …<br>
Selecting previously unselected package libgomp1:amd64.<br>
Preparing to unpack …/32-libgomp1_6.3.0-18+deb9u1_amd64.deb …<br>
Unpacking libgomp1:amd64 (6.3.0-18+deb9u1) …<br>
Selecting previously unselected package libitm1:amd64.<br>
Preparing to unpack …/33-libitm1_6.3.0-18+deb9u1_amd64.deb …<br>
Unpacking libitm1:amd64 (6.3.0-18+deb9u1) …<br>
Selecting previously unselected package libatomic1:amd64.<br>
Preparing to unpack …/34-libatomic1_6.3.0-18+deb9u1_amd64.deb …<br>
Unpacking libatomic1:amd64 (6.3.0-18+deb9u1) …<br>
Selecting previously unselected package libasan3:amd64.<br>
Preparing to unpack …/35-libasan3_6.3.0-18+deb9u1_amd64.deb …<br>
Unpacking libasan3:amd64 (6.3.0-18+deb9u1) …<br>
Selecting previously unselected package liblsan0:amd64.<br>
Preparing to unpack …/36-liblsan0_6.3.0-18+deb9u1_amd64.deb …<br>
Unpacking liblsan0:amd64 (6.3.0-18+deb9u1) …<br>
Selecting previously unselected package libtsan0:amd64.<br>
Preparing to unpack …/37-libtsan0_6.3.0-18+deb9u1_amd64.deb …<br>
Unpacking libtsan0:amd64 (6.3.0-18+deb9u1) …<br>
Selecting previously unselected package libubsan0:amd64.<br>
Preparing to unpack …/38-libubsan0_6.3.0-18+deb9u1_amd64.deb …<br>
Unpacking libubsan0:amd64 (6.3.0-18+deb9u1) …<br>
Selecting previously unselected package libcilkrts5:amd64.<br>
Preparing to unpack …/39-libcilkrts5_6.3.0-18+deb9u1_amd64.deb …<br>
Unpacking libcilkrts5:amd64 (6.3.0-18+deb9u1) …<br>
Selecting previously unselected package libmpx2:amd64.<br>
Preparing to unpack …/40-libmpx2_6.3.0-18+deb9u1_amd64.deb …<br>
Unpacking libmpx2:amd64 (6.3.0-18+deb9u1) …<br>
Selecting previously unselected package libquadmath0:amd64.<br>
Preparing to unpack …/41-libquadmath0_6.3.0-18+deb9u1_amd64.deb …<br>
Unpacking libquadmath0:amd64 (6.3.0-18+deb9u1) …<br>
Selecting previously unselected package libgcc-6-dev:amd64.<br>
Preparing to unpack …/42-libgcc-6-dev_6.3.0-18+deb9u1_amd64.deb …<br>
Unpacking libgcc-6-dev:amd64 (6.3.0-18+deb9u1) …<br>
Selecting previously unselected package gcc-6.<br>
Preparing to unpack …/43-gcc-6_6.3.0-18+deb9u1_amd64.deb …<br>
Unpacking gcc-6 (6.3.0-18+deb9u1) …<br>
Selecting previously unselected package gcc.<br>
Preparing to unpack …/44-gcc_4%3a6.3.0-4_amd64.deb …<br>
Unpacking gcc (4:6.3.0-4) …<br>
Selecting previously unselected package make.<br>
Preparing to unpack …/45-make_4.1-9.1_amd64.deb …<br>
Unpacking make (4.1-9.1) …<br>
Selecting previously unselected package patch.<br>
Preparing to unpack …/46-patch_2.7.5-1+deb9u2_amd64.deb …<br>
Unpacking patch (2.7.5-1+deb9u2) …<br>
Selecting previously unselected package dkms.<br>
Preparing to unpack …/47-dkms_2.3-2_all.deb …<br>
Unpacking dkms (2.3-2) …<br>
Selecting previously unselected package linux-kbuild-4.9.<br>
Preparing to unpack …/48-linux-kbuild-4.9_4.9.272-2_amd64.deb …<br>
Unpacking linux-kbuild-4.9 (4.9.272-2) …<br>
Selecting previously unselected package aufs-dkms.<br>
Preparing to unpack …/49-aufs-dkms_4.9+20161219-1_amd64.deb …<br>
Unpacking aufs-dkms (4.9+20161219-1) …<br>
Selecting previously unselected package aufs-tools.<br>
Preparing to unpack …/50-aufs-tools_1%3a4.1+20161219-1_amd64.deb …<br>
Unpacking aufs-tools (1:4.1+20161219-1) …<br>
Selecting previously unselected package cgroupfs-mount.<br>
Preparing to unpack …/51-cgroupfs-mount_1.3_all.deb …<br>
Unpacking cgroupfs-mount (1.3) …<br>
Selecting previously unselected package libseccomp2:amd64.<br>
Preparing to unpack …/52-libseccomp2_2.3.1-2.1+deb9u1_amd64.deb …<br>
Unpacking libseccomp2:amd64 (2.3.1-2.1+deb9u1) …<br>
Selecting previously unselected package <a href="http://containerd.io">containerd.io</a>.<br>
Preparing to unpack …/53-containerd.io_1.4.3-1_amd64.deb …<br>
Unpacking <a href="http://containerd.io">containerd.io</a> (1.4.3-1) …<br>
Selecting previously unselected package libdevmapper1.02.1:amd64.<br>
Preparing to unpack …/54-libdevmapper1.02.1_2%3a1.02.137-2_amd64.deb …<br>
Unpacking libdevmapper1.02.1:amd64 (2:1.02.137-2) …<br>
Selecting previously unselected package dmsetup.<br>
Preparing to unpack …/55-dmsetup_2%3a1.02.137-2_amd64.deb …<br>
Unpacking dmsetup (2:1.02.137-2) …<br>
Selecting previously unselected package docker-ce-cli.<br>
Preparing to unpack …/56-docker-ce-cli_5%3a19.03.15<sub>3-0</sub>debian-stretch_amd64.deb …<br>
Unpacking docker-ce-cli (5:19.03.15<sub>3-0</sub>debian-stretch) …<br>
Selecting previously unselected package docker-ce.<br>
Preparing to unpack …/57-docker-ce_5%3a19.03.15<sub>3-0</sub>debian-stretch_amd64.deb …<br>
Unpacking docker-ce (5:19.03.15<sub>3-0</sub>debian-stretch) …<br>
Selecting previously unselected package libfakeroot:amd64.<br>
Preparing to unpack …/58-libfakeroot_1.21-3.1_amd64.deb …<br>
Unpacking libfakeroot:amd64 (1.21-3.1) …<br>
Selecting previously unselected package fakeroot.<br>
Preparing to unpack …/59-fakeroot_1.21-3.1_amd64.deb …<br>
Unpacking fakeroot (1.21-3.1) …<br>
Selecting previously unselected package libc-dev-bin.<br>
Preparing to unpack …/60-libc-dev-bin_2.24-11+deb9u4_amd64.deb …<br>
Unpacking libc-dev-bin (2.24-11+deb9u4) …<br>
Selecting previously unselected package linux-libc-dev:amd64.<br>
Preparing to unpack …/61-linux-libc-dev_4.9.272-2_amd64.deb …<br>
Unpacking linux-libc-dev:amd64 (4.9.272-2) …<br>
Selecting previously unselected package libc6-dev:amd64.<br>
Preparing to unpack …/62-libc6-dev_2.24-11+deb9u4_amd64.deb …<br>
Unpacking libc6-dev:amd64 (2.24-11+deb9u4) …<br>
Selecting previously unselected package libltdl7:amd64.<br>
Preparing to unpack …/63-libltdl7_2.4.6-2_amd64.deb …<br>
Unpacking libltdl7:amd64 (2.4.6-2) …<br>
Selecting previously unselected package libxdmcp6:amd64.<br>
Preparing to unpack …/64-libxdmcp6_1%3a1.1.2-3_amd64.deb …<br>
Unpacking libxdmcp6:amd64 (1:1.1.2-3) …<br>
Selecting previously unselected package libxcb1:amd64.<br>
Preparing to unpack …/65-libxcb1_1.12-1_amd64.deb …<br>
Unpacking libxcb1:amd64 (1.12-1) …<br>
Selecting previously unselected package libx11-data.<br>
Preparing to unpack …/66-libx11-data_2%3a1.6.4-3+deb9u4_all.deb …<br>
Unpacking libx11-data (2:1.6.4-3+deb9u4) …<br>
Selecting previously unselected package libx11-6:amd64.<br>
Preparing to unpack …/67-libx11-6_2%3a1.6.4-3+deb9u4_amd64.deb …<br>
Unpacking libx11-6:amd64 (2:1.6.4-3+deb9u4) …<br>
Selecting previously unselected package libxext6:amd64.<br>
Preparing to unpack …/68-libxext6_2%3a1.3.3-1+b2_amd64.deb …<br>
Unpacking libxext6:amd64 (2:1.3.3-1+b2) …<br>
Selecting previously unselected package libxmuu1:amd64.<br>
Preparing to unpack …/69-libxmuu1_2%3a1.1.2-2_amd64.deb …<br>
Unpacking libxmuu1:amd64 (2:1.1.2-2) …<br>
Selecting previously unselected package linux-compiler-gcc-6-x86.<br>
Preparing to unpack …/70-linux-compiler-gcc-6-x86_4.9.272-2_amd64.deb …<br>
Unpacking linux-compiler-gcc-6-x86 (4.9.272-2) …<br>
Selecting previously unselected package linux-headers-4.9.0-16-common.<br>
Preparing to unpack …/71-linux-headers-4.9.0-16-common_4.9.272-2_all.deb …<br>
Unpacking linux-headers-4.9.0-16-common (4.9.272-2) …<br>
Selecting previously unselected package linux-headers-4.9.0-16-amd64.<br>
Preparing to unpack …/72-linux-headers-4.9.0-16-amd64_4.9.272-2_amd64.deb …<br>
Unpacking linux-headers-4.9.0-16-amd64 (4.9.272-2) …<br>
Selecting previously unselected package linux-headers-amd64.<br>
Preparing to unpack …/73-linux-headers-amd64_4.9+80+deb9u14_amd64.deb …<br>
Unpacking linux-headers-amd64 (4.9+80+deb9u14) …<br>
Selecting previously unselected package manpages-dev.<br>
Preparing to unpack …/74-manpages-dev_4.10-2_all.deb …<br>
Unpacking manpages-dev (4.10-2) …<br>
Selecting previously unselected package rename.<br>
Preparing to unpack …/75-rename_0.20-4_all.deb …<br>
Unpacking rename (0.20-4) …<br>
Selecting previously unselected package rsync.<br>
Preparing to unpack …/76-rsync_3.1.2-1+deb9u2_amd64.deb …<br>
Unpacking rsync (3.1.2-1+deb9u2) …<br>
Selecting previously unselected package strace.<br>
Preparing to unpack …/77-strace_4.15-2_amd64.deb …<br>
Unpacking strace (4.15-2) …<br>
Selecting previously unselected package sudo.<br>
Preparing to unpack …/78-sudo_1.8.19p1-2.1+deb9u3_amd64.deb …<br>
Unpacking sudo (1.8.19p1-2.1+deb9u3) …<br>
Selecting previously unselected package xauth.<br>
Preparing to unpack …/79-xauth_1%3a1.0.9-1+b2_amd64.deb …<br>
Unpacking xauth (1:1.0.9-1+b2) …<br>
Setting up libquadmath0:amd64 (6.3.0-18+deb9u1) …<br>
Setting up aufs-tools (1:4.1+20161219-1) …<br>
Setting up libgomp1:amd64 (6.3.0-18+deb9u1) …<br>
Setting up libatomic1:amd64 (6.3.0-18+deb9u1) …<br>
Setting up perl-modules-5.24 (5.24.1-3+deb9u7) …<br>
Setting up libgdbm3:amd64 (1.8.3-14) …<br>
Setting up manpages (4.10-2) …<br>
Setting up libperl5.24:amd64 (5.24.1-3+deb9u7) …<br>
Setting up strace (4.15-2) …<br>
Setting up git-man (1:2.11.0-3+deb9u7) …<br>
Setting up libpopt0:amd64 (1.16-10+b2) …<br>
Setting up libcc1-0:amd64 (6.3.0-18+deb9u1) …<br>
Setting up less (481-2.1) …<br>
debconf: unable to initialize frontend: Dialog<br>
debconf: (TERM is not set, so the dialog frontend is not usable.)<br>
debconf: falling back to frontend: Readline<br>
Setting up make (4.1-9.1) …<br>
Setting up libasan3:amd64 (6.3.0-18+deb9u1) …<br>
Setting up libip4tc0:amd64 (1.6.0+snapshot20161117-6) …<br>
Processing triggers for mime-support (3.60) …<br>
Setting up sudo (1.8.19p1-2.1+deb9u3) …<br>
Setting up libcilkrts5:amd64 (6.3.0-18+deb9u1) …<br>
Setting up libubsan0:amd64 (6.3.0-18+deb9u1) …<br>
Setting up libtsan0:amd64 (6.3.0-18+deb9u1) …<br>
Setting up linux-libc-dev:amd64 (4.9.272-2) …<br>
Setting up cgroupfs-mount (1.3) …<br>
invoke-rc.d: could not determine current runlevel<br>
invoke-rc.d: policy-rc.d denied execution of start.<br>
Setting up libbsd0:amd64 (0.8.3-1+deb9u1) …<br>
Setting up libkmod2:amd64 (23-2) …<br>
Setting up rsync (3.1.2-1+deb9u2) …<br>
invoke-rc.d: could not determine current runlevel<br>
invoke-rc.d: policy-rc.d denied execution of restart.<br>
Setting up liblsan0:amd64 (6.3.0-18+deb9u1) …<br>
Setting up perl (5.24.1-3+deb9u7) …<br>
update-alternatives: using /usr/bin/prename to provide /usr/bin/rename (rename) in auto mode<br>
Setting up libmpx2:amd64 (6.3.0-18+deb9u1) …<br>
Setting up libisl15:amd64 (0.18-1) …<br>
Setting up linux-headers-4.9.0-16-common (4.9.272-2) …<br>
Setting up patch (2.7.5-1+deb9u2) …<br>
Setting up linux-kbuild-4.9 (4.9.272-2) …<br>
Processing triggers for libc-bin (2.24-11+deb9u4) …<br>
Setting up libseccomp2:amd64 (2.3.1-2.1+deb9u1) …<br>
Setting up libxtables12:amd64 (1.6.0+snapshot20161117-6) …<br>
Setting up libfakeroot:amd64 (1.21-3.1) …<br>
Setting up libltdl7:amd64 (2.4.6-2) …<br>
Setting up libmpfr4:amd64 (3.1.5-1) …<br>
Setting up libnfnetlink0:amd64 (1.0.1-3) …<br>
Setting up libmpc3:amd64 (1.0.3-1+b2) …<br>
Setting up binutils (2.28-5) …<br>
Setting up cpp-6 (6.3.0-18+deb9u1) …<br>
Setting up libc-dev-bin (2.24-11+deb9u4) …<br>
Setting up libxdmcp6:amd64 (1:1.1.2-3) …<br>
Setting up docker-ce-cli (5:19.03.15<sub>3-0</sub>debian-stretch) …<br>
Setting up manpages-dev (4.10-2) …<br>
Setting up libc6-dev:amd64 (2.24-11+deb9u4) …<br>
Setting up pigz (2.3.4-1) …<br>
Setting up libitm1:amd64 (6.3.0-18+deb9u1) …<br>
Setting up libx11-data (2:1.6.4-3+deb9u4) …<br>
Setting up libxau6:amd64 (1:1.0.8-1) …<br>
Setting up libip6tc0:amd64 (1.6.0+snapshot20161117-6) …<br>
Setting up netbase (5.4) …<br>
Setting up libedit2:amd64 (3.1-20160903-3) …<br>
Setting up cpp (4:6.3.0-4) …<br>
Setting up kmod (23-2) …<br>
Setting up libgcc-6-dev:amd64 (6.3.0-18+deb9u1) …<br>
Setting up libnetfilter-conntrack3:amd64 (1.0.6-2) …<br>
Setting up <a href="http://containerd.io">containerd.io</a> (1.4.3-1) …<br>
Setting up liberror-perl (0.17024-1) …<br>
Setting up rename (0.20-4) …<br>
update-alternatives: using /usr/bin/file-rename to provide /usr/bin/rename (rename) in auto mode<br>
Setting up libiptc0:amd64 (1.6.0+snapshot20161117-6) …<br>
Setting up fakeroot (1.21-3.1) …<br>
update-alternatives: using /usr/bin/fakeroot-sysv to provide /usr/bin/fakeroot (fakeroot) in auto mode<br>
Setting up gcc-6 (6.3.0-18+deb9u1) …<br>
Setting up iptables (1.6.0+snapshot20161117-6) …<br>
Setting up openssh-client (1:7.4p1-10+deb9u7) …<br>
Setting up libxcb1:amd64 (1.12-1) …<br>
Setting up git (1:2.11.0-3+deb9u7) …<br>
Setting up libx11-6:amd64 (2:1.6.4-3+deb9u4) …<br>
Setting up gcc (4:6.3.0-4) …<br>
Setting up libxmuu1:amd64 (2:1.1.2-2) …<br>
Setting up linux-compiler-gcc-6-x86 (4.9.272-2) …<br>
Setting up dkms (2.3-2) …<br>
Setting up libxext6:amd64 (2:1.3.3-1+b2) …<br>
Setting up aufs-dkms (4.9+20161219-1) …<br>
Loading new aufs-4.9+20161219 DKMS files…<br>
It is likely that 4.18.0-240.el8.x86_64 belongs to a chroot’s host<br>
Building for 4.9.0-16-amd64<br>
Building initial module for 4.9.0-16-amd64<br>
Done.</p>
<p class="has-line-data" data-line-start="496" data-line-end="498">aufs:<br>
Running module version sanity check.</p>
<ul>
<li class="has-line-data" data-line-start="498" data-line-end="500">Original module
<ul>
<li class="has-line-data" data-line-start="499" data-line-end="500">No original module exists within this kernel</li>
</ul>
</li>
<li class="has-line-data" data-line-start="500" data-line-end="503">Installation
<ul>
<li class="has-line-data" data-line-start="501" data-line-end="503">Installing to /lib/modules/4.9.0-16-amd64/updates/dkms/</li>
</ul>
</li>
</ul>
<p class="has-line-data" data-line-start="503" data-line-end="504">depmod…</p>
<p class="has-line-data" data-line-start="505" data-line-end="519">DKMS: install completed.<br>
Setting up linux-headers-4.9.0-16-amd64 (4.9.272-2) …<br>
Setting up xauth (1:1.0.9-1+b2) …<br>
Setting up linux-headers-amd64 (4.9+80+deb9u14) …<br>
Setting up libdevmapper1.02.1:amd64 (2:1.02.137-2) …<br>
Setting up dmsetup (2:1.02.137-2) …<br>
Setting up docker-ce (5:19.03.15<sub>3-0</sub>debian-stretch) …<br>
invoke-rc.d: could not determine current runlevel<br>
invoke-rc.d: policy-rc.d denied execution of start.<br>
Processing triggers for libc-bin (2.24-11+deb9u4) …<br>
Removing intermediate container 5e527b7bbe50<br>
—&gt; 63a7f80783b4<br>
Successfully built 63a7f80783b4<br>
Successfully tagged obraz:base</p>
<pre><code class="has-line-data" data-line-start="520" data-line-end="522" class="language-sh">[root@localhost lesson1.<span class="hljs-number">5</span>]<span class="hljs-comment"># docker images</span>
</code></pre>
<p class="has-line-data" data-line-start="522" data-line-end="525">REPOSITORY   TAG       IMAGE ID       CREATED         SIZE<br>
obraz        base      63a7f80783b4   2 minutes ago   870MB<br>
debian       9         d74a4ce6ed8b   2 weeks ago     101MB</p>
<pre><code class="has-line-data" data-line-start="526" data-line-end="528" class="language-sh">[root@localhost lesson1.<span class="hljs-number">5</span>]<span class="hljs-comment"># ls</span>
</code></pre>
<p class="has-line-data" data-line-start="528" data-line-end="529">Dockerfile  Dockerfile3</p>
<pre><code class="has-line-data" data-line-start="530" data-line-end="532" class="language-sh">[root@localhost lesson1.<span class="hljs-number">5</span>]<span class="hljs-comment"># cat Dockerfile3</span>
</code></pre>
<p class="has-line-data" data-line-start="532" data-line-end="534">FROM debian:9<br>
RUN apt update &amp;&amp; apt install -y vim strace</p>
<pre><code class="has-line-data" data-line-start="535" data-line-end="538" class="language-sh">[root@localhost lesson1.<span class="hljs-number">5</span>]<span class="hljs-comment"># docker run -ti -d --privileged --mount</span>
<span class="hljs-built_in">type</span>=<span class="hljs-built_in">bind</span>,<span class="hljs-built_in">source</span>=<span class="hljs-string">"<span class="hljs-variable">$(pwd)</span>/Dockerfile3"</span>,target=/Dockerfile3 obraz:base
</code></pre>
<p class="has-line-data" data-line-start="538" data-line-end="539">efdd96a4e4e584436d36c2b33deaf39a5805572f2f3329bad71e8c134ea6c08e</p>
<pre><code class="has-line-data" data-line-start="540" data-line-end="542" class="language-sh">[root@localhost lesson1.<span class="hljs-number">5</span>]<span class="hljs-comment"># docker ps -a</span>
</code></pre>
<p class="has-line-data" data-line-start="542" data-line-end="545">CONTAINER ID   IMAGE          COMMAND                  CREATED         STATUS                     PORTS     NAMES<br>
efdd96a4e4e5   obraz:base     “bash”                   8 seconds ago   Up 7 seconds                         compassionate_lovelace<br>
d4bcf2a953aa   4c964184613d   “/bin/sh -c 'apt-get…”   7 minutes ago   Exited (0) 4 minutes ago             youthful_lamport</p>
<pre><code class="has-line-data" data-line-start="546" data-line-end="548" class="language-sh">[root@localhost lesson1.<span class="hljs-number">5</span>]<span class="hljs-comment"># docker exec -ti efdd96a4e4e5 bash</span>
</code></pre>
<pre><code class="has-line-data" data-line-start="549" data-line-end="551" class="language-sh">root@efdd96a4e4e5:/<span class="hljs-comment"># ps -aux</span>
</code></pre>
<p class="has-line-data" data-line-start="551" data-line-end="555">USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND<br>
root           1  0.0  0.1  18180  3176 pts/0    Ss+  18:59   0:00 bash<br>
root           7  0.0  0.1  18184  3152 pts/1    Ss   18:59   0:00 bash<br>
root          13  0.0  0.1  36636  2812 pts/1    R+   18:59   0:00 ps -aux</p>
<pre><code class="has-line-data" data-line-start="556" data-line-end="558" class="language-sh">root@efdd96a4e4e5:/<span class="hljs-comment"># service docker start</span>
</code></pre>
<p class="has-line-data" data-line-start="558" data-line-end="559">[ ok ] Starting Docker: docker.</p>
<pre><code class="has-line-data" data-line-start="560" data-line-end="562" class="language-sh">root@efdd96a4e4e5:/<span class="hljs-comment"># ls</span>
</code></pre>
<p class="has-line-data" data-line-start="562" data-line-end="563">Dockerfile3  bin  boot  dev  etc  home  lib  lib64  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var</p>
<pre><code class="has-line-data" data-line-start="564" data-line-end="566" class="language-sh">root@efdd96a4e4e5:/<span class="hljs-comment"># cat Dockerfile3 </span>
</code></pre>
<p class="has-line-data" data-line-start="567" data-line-end="569">FROM debian:9<br>
RUN apt update &amp;&amp; apt install -y vim strace</p>
<pre><code class="has-line-data" data-line-start="570" data-line-end="572" class="language-sh">root@efdd96a4e4e5:/<span class="hljs-comment"># cp Dockerfile3 /usr </span>
</code></pre>
<pre><code class="has-line-data" data-line-start="573" data-line-end="575" class="language-sh">root@efdd96a4e4e5:/<span class="hljs-comment"># cd usr/ </span>
</code></pre>
<pre><code class="has-line-data" data-line-start="576" data-line-end="578" class="language-sh">root@efdd96a4e4e5:/usr<span class="hljs-comment"># docker build -t obraz:final -f Dockerfile3 . </span>
</code></pre>
<p class="has-line-data" data-line-start="578" data-line-end="587">Sending build context to Docker daemon  838.3MB<br>
Step 1/2 : FROM debian:9<br>
9: Pulling from library/debian<br>
2f0ef4316716: Pull complete<br>
Digest: sha256:86269e614274db90a1d71dac258c74ed0a867a1d05f67dea6263b0f216ec7724<br>
Status: Downloaded newer image for debian:9<br>
—&gt; 5ddf6ebdcdb4<br>
Step 2/2 : RUN apt update &amp;&amp; apt install -y vim strace<br>
—&gt; Running in b26da75a540c</p>
<p class="has-line-data" data-line-start="590" data-line-end="602">Get:1 <a href="http://security.debian.org/debian-security">http://security.debian.org/debian-security</a> stretch/updates InRelease [53.0 kB]<br>
Ign:2 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch InRelease<br>
Get:3 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch-updates InRelease [93.6 kB]<br>
Get:4 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch Release [118 kB]<br>
Get:5 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch Release.gpg [3177 B]<br>
Get:6 <a href="http://security.debian.org/debian-security">http://security.debian.org/debian-security</a> stretch/updates/main amd64 Packages [721 kB]<br>
Get:7 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 Packages [7080 kB]<br>
Fetched 8068 kB in 3s (2037 kB/s)<br>
Reading package lists…<br>
Building dependency tree…<br>
Reading state information…<br>
All packages are up to date.</p>
<p class="has-line-data" data-line-start="603" data-line-end="604">WARNING: apt does not have a stable CLI interface. Use with caution in scripts.</p>
<p class="has-line-data" data-line-start="605" data-line-end="1124">Reading package lists…<br>
Building dependency tree…<br>
Reading state information…<br>
The following additional packages will be installed:<br>
libgpm2 vim-common vim-runtime xxd<br>
Suggested packages:<br>
gpm ctags vim-doc vim-scripts<br>
The following NEW packages will be installed:<br>
libgpm2 strace vim vim-common vim-runtime xxd<br>
0 upgraded, 6 newly installed, 0 to remove and 0 not upgraded.<br>
Need to get 7302 kB of archives.<br>
After this operation, 32.6 MB of additional disk space will be used.<br>
Get:1 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 xxd amd64 2:8.0.0197-4+deb9u3 [132 kB]<br>
Get:2 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 vim-common all 2:8.0.0197-4+deb9u3 [159 kB]<br>
Get:3 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 libgpm2 amd64 1.20.4-6.2+b1 [34.2 kB]<br>
Get:4 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 strace amd64 4.15-2 [533 kB]<br>
Get:5 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 vim-runtime all 2:8.0.0197-4+deb9u3 [5409 kB]<br>
Get:6 <a href="http://deb.debian.org/debian">http://deb.debian.org/debian</a> stretch/main amd64 vim amd64 2:8.0.0197-4+deb9u3 [1034 kB]<br>
debconf: delaying package configuration, since apt-utils is not installed<br>
Fetched 7302 kB in 2s (2472 kB/s)<br>
Selecting previously unselected package xxd.<br>
(Reading database … 6501 files and directories currently installed.)<br>
Preparing to unpack …/0-xxd_2%3a8.0.0197-4+deb9u3_amd64.deb …<br>
Unpacking xxd (2:8.0.0197-4+deb9u3) …<br>
Selecting previously unselected package vim-common.<br>
Preparing to unpack …/1-vim-common_2%3a8.0.0197-4+deb9u3_all.deb …<br>
Unpacking vim-common (2:8.0.0197-4+deb9u3) …<br>
Selecting previously unselected package libgpm2:amd64.<br>
Preparing to unpack …/2-libgpm2_1.20.4-6.2+b1_amd64.deb …<br>
Unpacking libgpm2:amd64 (1.20.4-6.2+b1) …<br>
Selecting previously unselected package strace.<br>
Preparing to unpack …/3-strace_4.15-2_amd64.deb …<br>
Unpacking strace (4.15-2) …<br>
Selecting previously unselected package vim-runtime.<br>
Preparing to unpack …/4-vim-runtime_2%3a8.0.0197-4+deb9u3_all.deb …<br>
Adding ‘diversion of /usr/share/vim/vim80/doc/help.txt to /usr/share/vim/vim80/doc/help.txt.vim-tiny by vim-runtime’<br>
Adding ‘diversion of /usr/share/vim/vim80/doc/tags to /usr/share/vim/vim80/doc/tags.vim-tiny by vim-runtime’<br>
Unpacking vim-runtime (2:8.0.0197-4+deb9u3) …<br>
Selecting previously unselected package vim.<br>
Preparing to unpack …/5-vim_2%3a8.0.0197-4+deb9u3_amd64.deb …<br>
Unpacking vim (2:8.0.0197-4+deb9u3) …<br>
Setting up strace (4.15-2) …<br>
Setting up xxd (2:8.0.0197-4+deb9u3) …<br>
Setting up libgpm2:amd64 (1.20.4-6.2+b1) …<br>
Processing triggers for libc-bin (2.24-11+deb9u4) …<br>
Setting up vim-common (2:8.0.0197-4+deb9u3) …<br>
Setting up vim-runtime (2:8.0.0197-4+deb9u3) …<br>
Setting up vim (2:8.0.0197-4+deb9u3) …<br>
update-alternatives: using /usr/bin/vim.basic to provide /usr/bin/vim (vim) in auto mode<br>
update-alternatives: using /usr/bin/vim.basic to provide /usr/bin/vimdiff (vimdiff) in auto mode<br>
update-alternatives: using /usr/bin/vim.basic to provide /usr/bin/rvim (rvim) in auto mode<br>
update-alternatives: using /usr/bin/vim.basic to provide /usr/bin/rview (rview) in auto mode<br>
update-alternatives: using /usr/bin/vim.basic to provide /usr/bin/vi (vi) in auto mode<br>
update-alternatives: using /usr/bin/vim.basic to provide /usr/bin/view (view) in auto mode<br>
update-alternatives: using /usr/bin/vim.basic to provide /usr/bin/ex (ex) in auto mode<br>
update-alternatives: using /usr/bin/vim.basic to provide /usr/bin/editor (editor) in auto mode<br>
Removing intermediate container b26da75a540c<br>
—&gt; 4e6a66771560<br>
Successfully built 4e6a66771560<br>
Successfully tagged obraz:final<br>
root@efdd96a4e4e5:/usr# ps -aux<br>
USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND<br>
root           1  0.0  0.0  18180   772 pts/0    Ss+  18:59   0:00 bash<br>
root           7  0.0  0.1  18184  2440 pts/1    Ss   18:59   0:00 bash<br>
root          57  4.0  6.4 513072 117584 ?       Sl   19:00   0:09 /usr/bin/dockerd -p /var/run/docker.pid<br>
root          79  0.2  1.4 568580 27284 ?        Ssl  19:00   0:00 containerd --config /var/run/docker/containerd/containerd.toml --log-level info<br>
root         578  0.0  0.1  36636  2792 pts/1    R+   19:04   0:00 ps -aux<br>
root@efdd96a4e4e5:/usr# strace docker<br>
execve(&quot;/usr/bin/docker&quot;, [“docker”], [/* 8 vars <em>/]) = 0<br>
brk(NULL)                               = 0x560a7d7a3000<br>
access(&quot;/etc/ld.so.nohwcap&quot;, F_OK)      = -1 ENOENT (No such file or directory)<br>
access(&quot;/etc/ld.so.preload&quot;, R_OK)      = -1 ENOENT (No such file or directory)<br>
open(&quot;/etc/ld.so.cache&quot;, O_RDONLY|O_CLOEXEC) = 3<br>
fstat(3, {st_mode=S_IFREG|0644, st_size=15516, …}) = 0<br>
mmap(NULL, 15516, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f61e7a51000<br>
close(3)                                = 0<br>
access(&quot;/etc/ld.so.nohwcap&quot;, F_OK)      = -1 ENOENT (No such file or directory)<br>
open(&quot;/lib/x86_64-linux-gnu/libpthread.so.0&quot;, O_RDONLY|O_CLOEXEC) = 3<br>
read(3, “\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0&gt;\0\1\0\0\0Pa\0\0\0\0\0\0”…, 832) = 832<br>
fstat(3, {st_mode=S_IFREG|0755, st_size=135440, …}) = 0<br>
mmap(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f61e7a4f000<br>
mmap(NULL, 2212936, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f61e7615000<br>
mprotect(0x7f61e762d000, 2093056, PROT_NONE) = 0<br>
mmap(0x7f61e782c000, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x17000) = 0x7f61e782c000<br>
mmap(0x7f61e782e000, 13384, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x7f61e782e000<br>
close(3)                                = 0<br>
access(&quot;/etc/ld.so.nohwcap&quot;, F_OK)      = -1 ENOENT (No such file or directory)<br>
open(&quot;/lib/x86_64-linux-gnu/libdl.so.2&quot;, O_RDONLY|O_CLOEXEC) = 3<br>
read(3, “\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0&gt;\0\1\0\0\0\200\r\0\0\0\0\0\0”…, 832) = 832<br>
fstat(3, {st_mode=S_IFREG|0644, st_size=14640, …}) = 0<br>
mmap(NULL, 2109680, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f61e7411000<br>
mprotect(0x7f61e7414000, 2093056, PROT_NONE) = 0<br>
mmap(0x7f61e7613000, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x2000) = 0x7f61e7613000<br>
close(3)                                = 0<br>
access(&quot;/etc/ld.so.nohwcap&quot;, F_OK)      = -1 ENOENT (No such file or directory)<br>
open(&quot;/lib/x86_64-linux-gnu/libc.so.6&quot;, O_RDONLY|O_CLOEXEC) = 3<br>
read(3, “\177ELF\2\1\1\3\0\0\0\0\0\0\0\0\3\0&gt;\0\1\0\0\0\0\4\2\0\0\0\0\0”…, 832) = 832<br>
fstat(3, {st_mode=S_IFREG|0755, st_size=1689360, …}) = 0<br>
mmap(NULL, 3795296, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f61e7072000<br>
mprotect(0x7f61e7207000, 2097152, PROT_NONE) = 0<br>
mmap(0x7f61e7407000, 24576, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x195000) = 0x7f61e7407000<br>
mmap(0x7f61e740d000, 14688, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x7f61e740d000<br>
close(3)                                = 0<br>
mmap(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f61e7a4d000<br>
arch_prctl(ARCH_SET_FS, 0x7f61e7a4d700) = 0<br>
mprotect(0x7f61e7407000, 16384, PROT_READ) = 0<br>
mprotect(0x7f61e7613000, 4096, PROT_READ) = 0<br>
mprotect(0x7f61e782c000, 4096, PROT_READ) = 0<br>
mprotect(0x560a7b2ae000, 23666688, PROT_READ) = 0<br>
mprotect(0x7f61e7a55000, 4096, PROT_READ) = 0<br>
munmap(0x7f61e7a51000, 15516)           = 0<br>
set_tid_address(0x7f61e7a4d9d0)         = 585<br>
set_robust_list(0x7f61e7a4d9e0, 24)     = 0<br>
rt_sigaction(SIGRTMIN, {sa_handler=0x7f61e761abd0, sa_mask=[], sa_flags=SA_RESTORER|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_1, {sa_handler=0x7f61e761ac60, sa_mask=[], sa_flags=SA_RESTORER|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigprocmask(SIG_UNBLOCK, [RTMIN RT_1], NULL, 8) = 0<br>
getrlimit(RLIMIT_STACK, {rlim_cur=8192</em>1024, rlim_max=RLIM64_INFINITY}) = 0<br>
brk(NULL)                               = 0x560a7d7a3000<br>
brk(0x560a7d7c4000)                     = 0x560a7d7c4000<br>
sched_getaffinity(0, 8192, [0])         = 16<br>
openat(AT_FDCWD, “/sys/kernel/mm/transparent_hugepage/hpage_pmd_size”, O_RDONLY) = 3<br>
read(3, “2097152\n”, 20)                = 8<br>
close(3)                                = 0<br>
mmap(NULL, 262144, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f61e7a0d000<br>
mmap(0xc000000000, 67108864, PROT_NONE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0xc000000000<br>
mmap(0xc000000000, 67108864, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0xc000000000<br>
mmap(NULL, 33554432, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f61e5072000<br>
mmap(NULL, 2164736, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f61e4e61000<br>
mmap(NULL, 65536, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f61e79fd000<br>
mmap(NULL, 65536, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f61e79ed000<br>
rt_sigprocmask(SIG_SETMASK, NULL, [], 8) = 0<br>
sigaltstack(NULL, {ss_sp=NULL, ss_flags=SS_DISABLE, ss_size=0}) = 0<br>
sigaltstack({ss_sp=0xc000002000, ss_flags=0, ss_size=32768}, NULL) = 0<br>
rt_sigprocmask(SIG_SETMASK, [], NULL, 8) = 0<br>
gettid()                                = 585<br>
rt_sigaction(SIGHUP, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGHUP, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGINT, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGINT, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGQUIT, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGQUIT, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGILL, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGILL, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGTRAP, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGTRAP, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGABRT, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGABRT, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGBUS, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGBUS, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGFPE, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGFPE, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGUSR1, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGUSR1, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGSEGV, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGSEGV, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGUSR2, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGUSR2, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGPIPE, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGPIPE, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGALRM, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGALRM, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGTERM, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGTERM, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGSTKFLT, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGSTKFLT, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGCHLD, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGCHLD, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGURG, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGURG, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGXCPU, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGXCPU, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGXFSZ, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGXFSZ, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGVTALRM, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGVTALRM, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGPROF, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGPROF, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGWINCH, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGWINCH, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGIO, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGIO, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGPWR, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGPWR, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGSYS, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGSYS, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRTMIN, NULL, {sa_handler=0x7f61e761abd0, sa_mask=[], sa_flags=SA_RESTORER|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, 8) = 0<br>
rt_sigaction(SIGRTMIN, NULL, {sa_handler=0x7f61e761abd0, sa_mask=[], sa_flags=SA_RESTORER|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, 8) = 0<br>
rt_sigaction(SIGRTMIN, {sa_handler=0x7f61e761abd0, sa_mask=[], sa_flags=SA_RESTORER|SA_STACK|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_1, NULL, {sa_handler=0x7f61e761ac60, sa_mask=[], sa_flags=SA_RESTORER|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, 8) = 0<br>
rt_sigaction(SIGRT_1, NULL, {sa_handler=0x7f61e761ac60, sa_mask=[], sa_flags=SA_RESTORER|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, 8) = 0<br>
rt_sigaction(SIGRT_1, {sa_handler=0x7f61e761ac60, sa_mask=[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_2, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_2, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_3, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_3, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_4, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_4, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_5, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_5, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_6, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_6, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_7, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_7, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_8, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_8, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_9, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_9, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_10, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_10, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_11, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_11, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_12, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_12, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_13, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_13, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_14, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_14, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_15, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_15, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_16, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_16, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_17, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_17, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_18, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_18, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_19, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_19, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_20, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_20, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_21, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_21, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_22, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_22, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_23, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_23, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_24, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_24, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_25, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_25, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_26, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_26, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_27, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_27, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_28, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_28, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_29, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_29, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_30, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_30, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_31, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_31, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigaction(SIGRT_32, NULL, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0<br>
rt_sigaction(SIGRT_32, {sa_handler=0x560a798c7580, sa_mask=~[], sa_flags=SA_RESTORER|SA_STACK|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f61e76260e0}, NULL, 8) = 0<br>
rt_sigprocmask(SIG_SETMASK, ~[RTMIN RT_1], [], 8) = 0<br>
mmap(NULL, 8392704, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS|MAP_STACK, -1, 0) = 0x7f61e4660000<br>
mprotect(0x7f61e4660000, 4096, PROT_NONE) = 0<br>
clone(child_stack=0x7f61e4e5fff0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7f61e4e609d0, tls=0x7f61e4e60700, child_tidptr=0x7f61e4e609d0) = 586<br>
rt_sigprocmask(SIG_SETMASK, [], NULL, 8) = 0<br>
rt_sigprocmask(SIG_SETMASK, ~[RTMIN RT_1], [], 8) = 0<br>
mmap(NULL, 8392704, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS|MAP_STACK, -1, 0) = 0x7f61e3e5f000<br>
mprotect(0x7f61e3e5f000, 4096, PROT_NONE) = 0<br>
clone(child_stack=0x7f61e465eff0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7f61e465f9d0, tls=0x7f61e465f700, child_tidptr=0x7f61e465f9d0) = 587<br>
rt_sigprocmask(SIG_SETMASK, [], NULL, 8) = 0<br>
futex(0x560a7c9ee768, FUTEX_WAIT_PRIVATE, 0, NULL) = 0<br>
futex(0xc00006e848, FUTEX_WAKE_PRIVATE, 1) = 1<br>
futex(0x560a7c9ee768, FUTEX_WAIT_PRIVATE, 0, NULL) = 0<br>
rt_sigprocmask(SIG_SETMASK, ~[RTMIN RT_1], [], 8) = 0<br>
mmap(NULL, 8392704, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS|MAP_STACK, -1, 0) = 0x7f61e365e000<br>
mprotect(0x7f61e365e000, 4096, PROT_NONE) = 0<br>
clone(child_stack=0x7f61e3e5dff0, flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID, parent_tidptr=0x7f61e3e5e9d0, tls=0x7f61e3e5e700, child_tidptr=0x7f61e3e5e9d0) = 588<br>
rt_sigprocmask(SIG_SETMASK, [], NULL, 8) = 0<br>
readlinkat(AT_FDCWD, “/proc/self/exe”, “/usr/bin/docker”, 128) = 15<br>
fcntl(0, F_GETFL)                       = 0x8002 (flags O_RDWR|O_LARGEFILE)<br>
mmap(NULL, 262144, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f61e79ad000<br>
fcntl(1, F_GETFL)                       = 0x8002 (flags O_RDWR|O_LARGEFILE)<br>
fcntl(2, F_GETFL)                       = 0x8002 (flags O_RDWR|O_LARGEFILE)<br>
madvise(0xc000000000, 2097152, MADV_NOHUGEPAGE) = 0<br>
madvise(0xc00014c000, 24576, MADV_FREE) = 0<br>
madvise(0xc000000000, 2097152, MADV_NOHUGEPAGE) = 0<br>
madvise(0xc000152000, 65536, MADV_FREE) = 0<br>
getpid()                                = 585<br>
newfstatat(AT_FDCWD, “/proc”, {st_mode=S_IFDIR|0555, st_size=0, …}, 0) = 0<br>
openat(AT_FDCWD, “/proc/stat”, O_RDONLY|O_CLOEXEC) = 3<br>
epoll_create1(EPOLL_CLOEXEC)            = 4<br>
epoll_ctl(4, EPOLL_CTL_ADD, 3, {EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, {u32=3885719304, u64=140058474274568}}) = 0<br>
fcntl(3, F_GETFL)                       = 0x8000 (flags O_RDONLY|O_LARGEFILE)<br>
fcntl(3, F_SETFL, O_RDONLY|O_NONBLOCK|O_LARGEFILE) = 0<br>
read(3, “cpu  330523 659 214912 31175002 “…, 4096) = 3214<br>
read(3, “”, 4096)                       = 0<br>
epoll_ctl(4, EPOLL_CTL_DEL, 3, 0xc0001c117c) = 0<br>
close(3)                                = 0<br>
futex(0xc00006e848, FUTEX_WAKE_PRIVATE, 1) = 1<br>
futex(0xc00006e848, FUTEX_WAKE_PRIVATE, 1) = 1<br>
futex(0xc00006e848, FUTEX_WAKE_PRIVATE, 1) = 1<br>
futex(0xc00006e848, FUTEX_WAKE_PRIVATE, 1) = 1<br>
getrandom(”\342\26\21\277kBr\23”, 8, 0) = 8<br>
newfstatat(AT_FDCWD, “/usr/lib/libykcs11.so”, 0xc0003085e8, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/usr/lib/libykcs11.so.1”, 0xc0003086b8, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/usr/lib64/libykcs11.so”, 0xc000308788, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/usr/lib64/libykcs11.so.1”, 0xc000308858, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/usr/lib/x86_64-linux-gnu/libykcs11.so”, 0xc000308928, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/usr/local/lib/libykcs11.so”, 0xc0003089f8, 0) = -1 ENOENT (No such file or directory)<br>
futex(0x560a7c9ee768, FUTEX_WAIT_PRIVATE, 0, NULL) = 0<br>
capget({version=0 /* <em>LINUX_CAPABILITY_VERSION</em>??? <em>/, pid=0}, NULL) = 0<br>
openat(AT_FDCWD, “/proc/sys/kernel/cap_last_cap”, O_RDONLY|O_CLOEXEC) = 3<br>
epoll_ctl(4, EPOLL_CTL_ADD, 3, {EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, {u32=3885719304, u64=140058474274568}}) = 0<br>
fcntl(3, F_GETFL)                       = 0x8000 (flags O_RDONLY|O_LARGEFILE)<br>
fcntl(3, F_SETFL, O_RDONLY|O_NONBLOCK|O_LARGEFILE) = 0<br>
read(3, “37\n”, 11)                     = 3<br>
epoll_ctl(4, EPOLL_CTL_DEL, 3, 0xc0001c1ba4) = 0<br>
close(3)                                = 0<br>
newfstatat(AT_FDCWD, “/usr/local/sbin/unpigz”, 0xc0003092e8, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/usr/local/bin/unpigz”, 0xc0003093b8, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/usr/sbin/unpigz”, 0xc000309488, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/usr/bin/unpigz”, {st_mode=S_IFREG|0755, st_size=117016, …}, 0) = 0<br>
getpid()                                = 585<br>
uname({sysname=“Linux”, nodename=“efdd96a4e4e5”, …}) = 0<br>
getuid()                                = 0<br>
socket(AF_UNIX, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK, 0) = 3<br>
connect(3, {sa_family=AF_UNIX, sun_path=&quot;/var/run/nscd/socket&quot;}, 110) = -1 ENOENT (No such file or directory)<br>
close(3)                                = 0<br>
socket(AF_UNIX, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK, 0) = 3<br>
connect(3, {sa_family=AF_UNIX, sun_path=&quot;/var/run/nscd/socket&quot;}, 110) = -1 ENOENT (No such file or directory)<br>
close(3)                                = 0<br>
open(&quot;/etc/nsswitch.conf&quot;, O_RDONLY|O_CLOEXEC) = 3<br>
fstat(3, {st_mode=S_IFREG|0644, st_size=497, …}) = 0<br>
read(3, “# /etc/nsswitch.conf\n#\n# Example”…, 4096) = 497<br>
read(3, “”, 4096)                       = 0<br>
close(3)                                = 0<br>
open(&quot;/etc/ld.so.cache&quot;, O_RDONLY|O_CLOEXEC) = 3<br>
fstat(3, {st_mode=S_IFREG|0644, st_size=15516, …}) = 0<br>
mmap(NULL, 15516, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f61e7a51000<br>
close(3)                                = 0<br>
access(&quot;/etc/ld.so.nohwcap&quot;, F_OK)      = -1 ENOENT (No such file or directory)<br>
open(&quot;/lib/x86_64-linux-gnu/libnss_compat.so.2&quot;, O_RDONLY|O_CLOEXEC) = 3<br>
read(3, “\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0&gt;\0\1\0\0\0\260\22\0\0\0\0\0\0”…, 832) = 832<br>
fstat(3, {st_mode=S_IFREG|0644, st_size=31616, …}) = 0<br>
mmap(NULL, 2126944, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f61e2c55000<br>
mprotect(0x7f61e2c5c000, 2093056, PROT_NONE) = 0<br>
mmap(0x7f61e2e5b000, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x6000) = 0x7f61e2e5b000<br>
close(3)                                = 0<br>
access(&quot;/etc/ld.so.nohwcap&quot;, F_OK)      = -1 ENOENT (No such file or directory)<br>
open(&quot;/lib/x86_64-linux-gnu/libnsl.so.1&quot;, O_RDONLY|O_CLOEXEC) = 3<br>
read(3, “\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0&gt;\0\1\0\0\0\320?\0\0\0\0\0\0”…, 832) = 832<br>
fstat(3, {st_mode=S_IFREG|0644, st_size=89064, …}) = 0<br>
mmap(NULL, 2194008, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f61e2a3d000<br>
mprotect(0x7f61e2a51000, 2097152, PROT_NONE) = 0<br>
mmap(0x7f61e2c51000, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x14000) = 0x7f61e2c51000<br>
mmap(0x7f61e2c53000, 6744, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x7f61e2c53000<br>
close(3)                                = 0<br>
mprotect(0x7f61e2c51000, 4096, PROT_READ) = 0<br>
mprotect(0x7f61e2e5b000, 4096, PROT_READ) = 0<br>
munmap(0x7f61e7a51000, 15516)           = 0<br>
open(&quot;/etc/ld.so.cache&quot;, O_RDONLY|O_CLOEXEC) = 3<br>
fstat(3, {st_mode=S_IFREG|0644, st_size=15516, …}) = 0<br>
mmap(NULL, 15516, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f61e7a51000<br>
close(3)                                = 0<br>
access(&quot;/etc/ld.so.nohwcap&quot;, F_OK)      = -1 ENOENT (No such file or directory)<br>
open(&quot;/lib/x86_64-linux-gnu/libnss_nis.so.2&quot;, O_RDONLY|O_CLOEXEC) = 3<br>
read(3, “\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0&gt;\0\1\0\0\0\340 \0\0\0\0\0\0”…, 832) = 832<br>
fstat(3, {st_mode=S_IFREG|0644, st_size=47688, …}) = 0<br>
mmap(NULL, 2143656, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f61e2831000<br>
mprotect(0x7f61e283c000, 2093056, PROT_NONE) = 0<br>
mmap(0x7f61e2a3b000, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0xa000) = 0x7f61e2a3b000<br>
close(3)                                = 0<br>
access(&quot;/etc/ld.so.nohwcap&quot;, F_OK)      = -1 ENOENT (No such file or directory)<br>
open(&quot;/lib/x86_64-linux-gnu/libnss_files.so.2&quot;, O_RDONLY|O_CLOEXEC) = 3<br>
read(3, “\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0&gt;\0\1\0\0\0\320!\0\0\0\0\0\0”…, 832) = 832<br>
fstat(3, {st_mode=S_IFREG|0644, st_size=47632, …}) = 0<br>
mmap(NULL, 2168600, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f61e261f000<br>
mprotect(0x7f61e2629000, 2097152, PROT_NONE) = 0<br>
mmap(0x7f61e2829000, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0xa000) = 0x7f61e2829000<br>
mmap(0x7f61e282b000, 22296, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x7f61e282b000<br>
close(3)                                = 0<br>
mprotect(0x7f61e2829000, 4096, PROT_READ) = 0<br>
mprotect(0x7f61e2a3b000, 4096, PROT_READ) = 0<br>
munmap(0x7f61e7a51000, 15516)           = 0<br>
open(&quot;/etc/passwd&quot;, O_RDONLY|O_CLOEXEC) = 3<br>
lseek(3, 0, SEEK_CUR)                   = 0<br>
fstat(3, {st_mode=S_IFREG|0644, st_size=919, …}) = 0<br>
mmap(NULL, 919, PROT_READ, MAP_SHARED, 3, 0) = 0x7f61e7a54000<br>
lseek(3, 919, SEEK_SET)                 = 919<br>
munmap(0x7f61e7a54000, 919)             = 0<br>
close(3)                                = 0<br>
futex(0xc0000fd648, FUTEX_WAKE_PRIVATE, 1) = 1<br>
futex(0x560a7c9ee768, FUTEX_WAIT_PRIVATE, 0, NULL) = 0<br>
futex(0xc0003ae848, FUTEX_WAKE_PRIVATE, 1) = 1<br>
futex(0x560a7c9eded8, FUTEX_WAIT_PRIVATE, 0, NULL) = 0<br>
epoll_pwait(4, [], 128, 0, NULL, 32)    = 0<br>
mmap(NULL, 65536, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f61e799d000<br>
epoll_pwait(4, [], 128, 0, NULL, 18446744073709551615) = 0<br>
futex(0x560a7c9edab0, FUTEX_WAKE_PRIVATE, 1) = 1<br>
ioctl(0, TCGETS, {B38400 opost isig icanon echo …}) = 0<br>
ioctl(1, TCGETS, {B38400 opost isig icanon echo …}) = 0<br>
newfstatat(AT_FDCWD, “/root/.docker/config.json”, 0xc0002653b8, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/root/.dockercfg”, 0xc000265488, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/usr/local/sbin/pass”, 0xc000265558, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/usr/local/bin/pass”, 0xc000265628, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/usr/sbin/pass”, 0xc0002656f8, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/usr/bin/pass”, 0xc0002657c8, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/sbin/pass”, 0xc000265898, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/bin/pass”, 0xc000265968, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/usr/local/sbin/docker-credential-secretservice”, 0xc000265a38, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/usr/local/bin/docker-credential-secretservice”, 0xc000265b08, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/usr/sbin/docker-credential-secretservice”, 0xc000265bd8, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/usr/bin/docker-credential-secretservice”, 0xc000265ca8, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/sbin/docker-credential-secretservice”, 0xc000265d78, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/bin/docker-credential-secretservice”, 0xc000265e48, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/root/.kube/config”, 0xc000265f18, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/root/.kube/config”, 0xc0002a8038, 0) = -1 ENOENT (No such file or directory)<br>
socket(AF_UNIX, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK, 0) = 3<br>
setsockopt(3, SOL_SOCKET, SO_BROADCAST, [1], 4) = 0<br>
connect(3, {sa_family=AF_UNIX, sun_path=&quot;/var/run/docker.sock&quot;}, 23) = 0<br>
epoll_ctl(4, EPOLL_CTL_ADD, 3, {EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, {u32=3885719304, u64=140058474274568}}) = 0<br>
getsockname(3, {sa_family=AF_UNIX}, [112-&gt;2]) = 0<br>
getpeername(3, {sa_family=AF_UNIX, sun_path=&quot;/var/run/docker.sock&quot;}, [112-&gt;23]) = 0<br>
read(3, 0xc00025f000, 4096)             = -1 EAGAIN (Resource temporarily unavailable)<br>
write(3, “HEAD /_ping HTTP/1.1\r\nHost: dock”…, 82) = 82<br>
epoll_pwait(4, [{EPOLLIN|EPOLLOUT, {u32=3885719304, u64=140058474274568}}], 128, 0, NULL, 824637415688) = 1<br>
read(3, “HTTP/1.1 200 OK\r\nApi-Version: 1.”…, 4096) = 281<br>
read(3, 0xc00025f000, 4096)             = -1 EAGAIN (Resource temporarily unavailable)<br>
newfstatat(AT_FDCWD, “/root/.docker/cli-plugins”, 0xc0002a81d8, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/usr/local/lib/docker/cli-plugins”, 0xc0002a82a8, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/usr/local/libexec/docker/cli-plugins”, 0xc0002a8378, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/usr/lib/docker/cli-plugins”, 0xc0002a8448, 0) = -1 ENOENT (No such file or directory)<br>
newfstatat(AT_FDCWD, “/usr/libexec/docker/cli-plugins”, {st_mode=S_IFDIR|0755, st_size=45, …}, 0) = 0<br>
futex(0x560a7c9edab0, FUTEX_WAKE_PRIVATE, 1) = 1<br>
openat(AT_FDCWD, “/usr/libexec/docker/cli-plugins”, O_RDONLY|O_CLOEXEC) = 5<br>
epoll_ctl(4, EPOLL_CTL_ADD, 5, {EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, {u32=3885719096, u64=140058474274360}}) = -1 EPERM (Operation not permitted)<br>
epoll_ctl(4, EPOLL_CTL_DEL, 5, 0xc0006113b4) = -1 EPERM (Operation not permitted)<br>
getdents64(5, /</em> 4 entries <em>/, 8192)    = 120<br>
getdents64(5, /</em> 0 entries */, 8192)    = 0<br>
newfstatat(AT_FDCWD, “/usr/libexec/docker/cli-plugins/docker-app”, {st_mode=S_IFREG|0755, st_size=42021432, …}, AT_SYMLINK_NOFOLLOW) = 0<br>
newfstatat(AT_FDCWD, “/usr/libexec/docker/cli-plugins/docker-buildx”, {st_mode=S_IFREG|0755, st_size=60129344, …}, AT_SYMLINK_NOFOLLOW) = 0<br>
close(5)                                = 0<br>
openat(AT_FDCWD, “/dev/null”, O_RDONLY|O_CLOEXEC) = 5<br>
epoll_ctl(4, EPOLL_CTL_ADD, 5, {EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, {u32=3885719096, u64=140058474274360}}) = -1 EPERM (Operation not permitted)<br>
epoll_ctl(4, EPOLL_CTL_DEL, 5, 0xc000611154) = -1 EPERM (Operation not permitted)<br>
pipe2([6, 7], O_CLOEXEC)                = 0<br>
epoll_ctl(4, EPOLL_CTL_ADD, 6, {EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, {u32=3885719096, u64=140058474274360}}) = 0<br>
fcntl(6, F_GETFL)                       = 0 (flags O_RDONLY)<br>
fcntl(6, F_SETFL, O_RDONLY|O_NONBLOCK)  = 0<br>
epoll_ctl(4, EPOLL_CTL_ADD, 7, {EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, {u32=3885718888, u64=140058474274152}}) = 0<br>
fcntl(7, F_GETFL)                       = 0x1 (flags O_WRONLY)<br>
fcntl(7, F_SETFL, O_WRONLY|O_NONBLOCK)  = 0<br>
pipe2([8, 9], O_CLOEXEC)                = 0<br>
epoll_ctl(4, EPOLL_CTL_ADD, 8, {EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, {u32=3885718680, u64=140058474273944}}) = 0<br>
fcntl(8, F_GETFL)                       = 0 (flags O_RDONLY)<br>
fcntl(8, F_SETFL, O_RDONLY|O_NONBLOCK)  = 0<br>
epoll_ctl(4, EPOLL_CTL_ADD, 9, {EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, {u32=3885718472, u64=140058474273736}}) = 0<br>
fcntl(9, F_GETFL)                       = 0x1 (flags O_WRONLY)<br>
fcntl(9, F_SETFL, O_WRONLY|O_NONBLOCK)  = 0<br>
fcntl(7, F_GETFL)                       = 0x801 (flags O_WRONLY|O_NONBLOCK)<br>
fcntl(7, F_SETFL, O_WRONLY)             = 0<br>
fcntl(9, F_GETFL)                       = 0x801 (flags O_WRONLY|O_NONBLOCK)<br>
fcntl(9, F_SETFL, O_WRONLY)             = 0<br>
pipe2([10, 11], O_CLOEXEC)              = 0<br>
getpid()                                = 585<br>
rt_sigprocmask(SIG_SETMASK, NULL, [], 8) = 0<br>
rt_sigprocmask(SIG_SETMASK, ~[], NULL, 8) = 0<br>
clone(child_stack=NULL, flags=CLONE_VM|CLONE_VFORK|SIGCHLD) = 591<br>
rt_sigprocmask(SIG_SETMASK, [], NULL, 8) = 0<br>
close(11)                               = 0<br>
read(10, “”, 8)                         = 0<br>
close(10)                               = 0<br>
close(5)                                = 0<br>
epoll_ctl(4, EPOLL_CTL_DEL, 7, 0xc0006111ec) = 0<br>
close(7)                                = 0<br>
epoll_ctl(4, EPOLL_CTL_DEL, 9, 0xc0006111ec) = 0<br>
close(9)                                = 0<br>
waitid(P_PID, 591, {si_signo=SIGCHLD, si_code=CLD_EXITED, si_pid=591, si_uid=0, si_status=0, si_utime=0, si_stime=0}, WEXITED|WNOWAIT, NULL) = 0<br>
— SIGCHLD {si_signo=SIGCHLD, si_code=CLD_EXITED, si_pid=591, si_uid=0, si_status=0, si_utime=0, si_stime=1} —<br>
rt_sigreturn({mask=[]})                 = 0<br>
wait4(591, [{WIFEXITED(s) &amp;&amp; WEXITSTATUS(s) == 0}], 0, {ru_utime={tv_sec=0, tv_usec=12050}, ru_stime={tv_sec=0, tv_usec=44322}, …}) = 591<br>
openat(AT_FDCWD, “/dev/null”, O_RDONLY|O_CLOEXEC) = 5<br>
epoll_ctl(4, EPOLL_CTL_ADD, 5, {EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, {u32=3885718680, u64=140058474273944}}) = -1 EPERM (Operation not permitted)<br>
epoll_ctl(4, EPOLL_CTL_DEL, 5, 0xc000611154) = -1 EPERM (Operation not permitted)<br>
pipe2([6, 7], O_CLOEXEC)                = 0<br>
epoll_ctl(4, EPOLL_CTL_ADD, 6, {EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, {u32=3885718680, u64=140058474273944}}) = 0<br>
fcntl(6, F_GETFL)                       = 0 (flags O_RDONLY)<br>
fcntl(6, F_SETFL, O_RDONLY|O_NONBLOCK)  = 0<br>
epoll_ctl(4, EPOLL_CTL_ADD, 7, {EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, {u32=3885719096, u64=140058474274360}}) = 0<br>
fcntl(7, F_GETFL)                       = 0x1 (flags O_WRONLY)<br>
fcntl(7, F_SETFL, O_WRONLY|O_NONBLOCK)  = 0<br>
pipe2([8, 9], O_CLOEXEC)                = 0<br>
epoll_ctl(4, EPOLL_CTL_ADD, 8, {EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, {u32=3885718472, u64=140058474273736}}) = 0<br>
fcntl(8, F_GETFL)                       = 0 (flags O_RDONLY)<br>
fcntl(8, F_SETFL, O_RDONLY|O_NONBLOCK)  = 0<br>
epoll_ctl(4, EPOLL_CTL_ADD, 9, {EPOLLIN|EPOLLOUT|EPOLLRDHUP|EPOLLET, {u32=3885718888, u64=140058474274152}}) = 0<br>
fcntl(9, F_GETFL)                       = 0x1 (flags O_WRONLY)<br>
fcntl(9, F_SETFL, O_WRONLY|O_NONBLOCK)  = 0<br>
fcntl(7, F_GETFL)                       = 0x801 (flags O_WRONLY|O_NONBLOCK)<br>
fcntl(7, F_SETFL, O_WRONLY)             = 0<br>
fcntl(9, F_GETFL)                       = 0x801 (flags O_WRONLY|O_NONBLOCK)<br>
fcntl(9, F_SETFL, O_WRONLY)             = 0<br>
pipe2([10, 11], O_CLOEXEC)              = 0<br>
getpid()                                = 585<br>
rt_sigprocmask(SIG_SETMASK, NULL, [], 8) = 0<br>
rt_sigprocmask(SIG_SETMASK, ~[], NULL, 8) = 0<br>
clone(child_stack=NULL, flags=CLONE_VM|CLONE_VFORK|SIGCHLD) = 598<br>
rt_sigprocmask(SIG_SETMASK, [], NULL, 8) = 0<br>
close(11)                               = 0<br>
read(10, “”, 8)                         = 0<br>
close(10)                               = 0<br>
close(5)                                = 0<br>
epoll_ctl(4, EPOLL_CTL_DEL, 7, 0xc0006111ec) = 0<br>
close(7)                                = 0<br>
epoll_ctl(4, EPOLL_CTL_DEL, 9, 0xc0006111ec) = 0<br>
close(9)                                = 0<br>
waitid(P_PID, 598, {si_signo=SIGCHLD, si_code=CLD_EXITED, si_pid=598, si_uid=0, si_status=0, si_utime=0, si_stime=0}, WEXITED|WNOWAIT, NULL) = 0<br>
— SIGCHLD {si_signo=SIGCHLD, si_code=CLD_EXITED, si_pid=598, si_uid=0, si_status=0, si_utime=0, si_stime=1} —<br>
rt_sigreturn({mask=[]})                 = 0<br>
wait4(598, [{WIFEXITED(s) &amp;&amp; WEXITSTATUS(s) == 0}], 0, {ru_utime={tv_sec=0, tv_usec=14054}, ru_stime={tv_sec=0, tv_usec=31018}, …}) = 598<br>
write(2, “\n”, 1<br>
)                       = 1<br>
epoll_pwait(4, [], 128, 0, NULL, 4)     = 0<br>
ioctl(0, TIOCGWINSZ, {ws_row=41, ws_col=182, ws_xpixel=0, ws_ypixel=0}) = 0<br>
mmap(NULL, 65536, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f61e798d000<br>
write(2, “Usage:\tdocker [OPTIONS] COMMAND\n”…, 3842Usage:    docker [OPTIONS] COMMAND</p>
<p class="has-line-data" data-line-start="1125" data-line-end="1126">A self-sufficient runtime for containers</p>
<p class="has-line-data" data-line-start="1127" data-line-end="1139">Options:<br>
–config string      Location of client config files (default “/root/.docker”)<br>
-c, --context string     Name of the context to use to connect to the daemon (overrides DOCKER_HOST env var and default context set with “docker context use”)<br>
-D, --debug              Enable debug mode<br>
-H, --host list          Daemon socket(s) to connect to<br>
-l, --log-level string   Set the logging level (“debug”|“info”|“warn”|“error”|“fatal”) (default “info”)<br>
–tls                Use TLS; implied by --tlsverify<br>
–tlscacert string   Trust certs signed only by this CA (default “/root/.docker/ca.pem”)<br>
–tlscert string     Path to TLS certificate file (default “/root/.docker/cert.pem”)<br>
–tlskey string      Path to TLS key file (default “/root/.docker/key.pem”)<br>
–tlsverify          Use TLS and verify the remote<br>
-v, --version            Print version information and quit</p>
<p class="has-line-data" data-line-start="1140" data-line-end="1157">Management Commands:<br>
builder     Manage builds<br>
config      Manage Docker configs<br>
container   Manage containers<br>
context     Manage contexts<br>
engine      Manage the docker engine<br>
image       Manage images<br>
network     Manage networks<br>
node        Manage Swarm nodes<br>
plugin      Manage plugins<br>
secret      Manage Docker secrets<br>
service     Manage services<br>
stack       Manage Docker stacks<br>
swarm       Manage Swarm<br>
system      Manage Docker<br>
trust       Manage trust on Docker images<br>
volume      Manage volumes</p>
<p class="has-line-data" data-line-start="1158" data-line-end="1199">Commands:<br>
attach      Attach local standard input, output, and error streams to a running container<br>
build       Build an image from a Dockerfile<br>
commit      Create a new image from a container’s changes<br>
cp          Copy files/folders between a container and the local filesystem<br>
create      Create a new container<br>
diff        Inspect changes to files or directories on a container’s filesystem<br>
events      Get real time events from the server<br>
exec        Run a command in a running container<br>
export      Export a container’s filesystem as a tar archive<br>
history     Show the history of an image<br>
images      List images<br>
import      Import the contents from a tarball to create a filesystem image<br>
info        Display system-wide information<br>
inspect     Return low-level information on Docker objects<br>
kill        Kill one or more running containers<br>
load        Load an image from a tar archive or STDIN<br>
login       Log in to a Docker registry<br>
logout      Log out from a Docker registry<br>
logs        Fetch the logs of a container<br>
pause       Pause all processes within one or more containers<br>
port        List port mappings or a specific mapping for the container<br>
ps          List containers<br>
pull        Pull an image or a repository from a registry<br>
push        Push an image or a repository to a registry<br>
rename      Rename a container<br>
restart     Restart one or more containers<br>
rm          Remove one or more containers<br>
rmi         Remove one or more images<br>
run         Run a command in a new container<br>
save        Save one or more images to a tar archive (streamed to STDOUT by default)<br>
search      Search the Docker Hub for images<br>
start       Start one or more stopped containers<br>
stats       Display a live stream of container(s) resource usage statistics<br>
stop        Stop one or more running containers<br>
tag         Create a tag TARGET_IMAGE that refers to SOURCE_IMAGE<br>
top         Display the running processes of a container<br>
unpause     Unpause all processes within one or more containers<br>
update      Update configuration of one or more containers<br>
version     Show the Docker version information<br>
wait        Block until one or more containers stop, then print their exit codes</p>
<p class="has-line-data" data-line-start="1200" data-line-end="1204">Run ‘docker COMMAND --help’ for more information on a command.<br>
) = 3842<br>
exit_group(0)                           = ?<br>
+++ exited with 0 +++</p>
<pre><code class="has-line-data" data-line-start="1205" data-line-end="1207" class="language-sh">root@efdd96a4e4e5:/usr<span class="hljs-comment"># ps -aux </span>
</code></pre>
<p class="has-line-data" data-line-start="1207" data-line-end="1213">USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND<br>
root           1  0.0  0.0  18180   772 pts/0    Ss+  18:59   0:00 bash<br>
root           7  0.0  0.1  18192  2448 pts/1    Ss   18:59   0:00 bash<br>
root          57  3.2  6.4 513072 117584 ?       Sl   19:00   0:10 /usr/bin/dockerd -p /var/run/docker.pid<br>
root          79  0.2  1.4 568580 27284 ?        Ssl  19:00   0:00 containerd --config /var/run/docker/containerd/containerd.toml --log-level info<br>
root         683  0.0  0.1  36636  2836 pts/1    R+   19:05   0:00 ps -aux</p>
<pre><code class="has-line-data" data-line-start="1214" data-line-end="1216" class="language-sh">root@efdd96a4e4e5:/usr<span class="hljs-comment"># docker images </span>
</code></pre>
<p class="has-line-data" data-line-start="1216" data-line-end="1219">REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE<br>
obraz               final               4e6a66771560        3 minutes ago       149MB<br>
debian              9                   5ddf6ebdcdb4        42 hours ago        101MB</p>
<pre><code class="has-line-data" data-line-start="1220" data-line-end="1222" class="language-sh">root@efdd96a4e4e5:/usr<span class="hljs-comment"># docker run -ti obraz:final </span>
</code></pre>
<pre><code class="has-line-data" data-line-start="1223" data-line-end="1225" class="language-sh">root@<span class="hljs-number">44</span>f9e414130e:/<span class="hljs-comment"># ps -aux</span>
</code></pre>
<p class="has-line-data" data-line-start="1225" data-line-end="1226">bash: ps: command not found</p>
<pre><code class="has-line-data" data-line-start="1227" data-line-end="1229" class="language-sh">root@<span class="hljs-number">44</span>f9e414130e:/<span class="hljs-comment"># pwd</span>
</code></pre>
<p class="has-line-data" data-line-start="1229" data-line-end="1230">/</p>
<pre><code class="has-line-data" data-line-start="1231" data-line-end="1233" class="language-sh">root@<span class="hljs-number">44</span>f9e414130e:/<span class="hljs-comment"># ls </span>
</code></pre>
<p class="has-line-data" data-line-start="1233" data-line-end="1234">bin  boot  dev  etc  home  lib  lib64  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var</p>
<pre><code class="has-line-data" data-line-start="1235" data-line-end="1237" class="language-sh">root@<span class="hljs-number">44</span>f9e414130e:/<span class="hljs-comment"># exit</span>
</code></pre>
<p class="has-line-data" data-line-start="1237" data-line-end="1238">exit</p>
<pre><code class="has-line-data" data-line-start="1239" data-line-end="1241" class="language-sh">root@efdd96a4e4e5:/usr<span class="hljs-comment"># exit</span>
</code></pre>
<p class="has-line-data" data-line-start="1241" data-line-end="1242">exit</p>
<pre><code class="has-line-data" data-line-start="1243" data-line-end="1245" class="language-sh">[root@localhost lesson1.<span class="hljs-number">5</span>]<span class="hljs-comment"># pwd </span>
</code></pre>
<p class="has-line-data" data-line-start="1245" data-line-end="1246">/root/lesson1.5</p>
<pre><code class="has-line-data" data-line-start="1247" data-line-end="1249" class="language-sh">[root@localhost lesson1.<span class="hljs-number">5</span>]<span class="hljs-comment"># </span>
</code></pre>
